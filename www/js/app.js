var endpoint_url = 'http://192.168.0.102';
//var endpoint_url = 'http://test.infoad.tv';
//var endpoint_url = 'http://127.0.0.1:8005';

var appConfig = {
    push: true,
    device: 'mobile',
    debug: false
};

var uuid = undefined;

angular.module('infoad', [
        'ionic',
        'ngCordova',
        'jett.ionic.filter.bar',
        'infoad.menu.controllers',
        'infoad.utils',
        'infoad.auth',
        'infoad.segmento',
        'infoad.tag'
    ])

    .run(function($ionicPlatform, $rootScope, $state, $http, AuthenticationStorage, $cordovaDevice) {
        $ionicPlatform.onHardwareBackButton(function(event){
            if($state.current.name == 'app.segmentos-search') {
                event.preventDefault(); event.stopPropagation();
                $rootScope.$broadcast('change.textSearch', '');
            }
        });

        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            if( appConfig.device != 'web') {
                    uuid = $cordovaDevice.getUUID().substr(0,14);
                } else {
                    uuid = 55;
                }
        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {

                if (toState.data.requireLogin && AuthenticationStorage.is_authenticated() == false){
                    event.preventDefault();
                    return $state.go('login', { "redirect": toState.name});
                } else {
                    // Unless we are navigating to the login, attached Authorization Token to every Request
                    if (toState.name != 'login' && AuthenticationStorage.is_authenticated()) {
                        $http.defaults.headers.common.Authorization = 'Token ' + AuthenticationStorage.get_token();
                    }/*else if(toState.name == 'login' && AuthenticationStorage.is_authenticated()){
                        $state.go('app.segmentos-search');
                    }*/
                }
            });
    })

    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'MenuController as vm'
            })
            .state('app.tag', {
                url: '/my-tags',
                data: { requireLogin: true},
                views: {
                    'menuContent': {
                        templateUrl: 'js/modules/tag/templates/list.html',
                        controller: 'TagListCtrl as vm'
                    }
                }
            })
            .state('app.segmento-detail', {
                    data: { requireLogin: true},
                    url: '/segmentos/:id',
                    views:{
                        'menuContent': {
                            templateUrl: 'js/modules/segmento/templates/detail.html',
                            controller: 'SegmentDetailCtrl as vm'
                        }
                    }
                })
            .state('app.segmentos-search', {
                url: '/segmentos-search',
                data: { requireLogin: true},
                views: {
                    'menuContent': {
                        templateUrl: 'js/modules/segmento/templates/search.html',
                        controller: 'SegmentosSearchListCtrl as vm'
                    }
                }
            })
            .state('login', {
                url: '/login',
                data: { requireLogin: false},
                templateUrl: 'js/modules/auth/templates/login.html',
                controller: 'LoginCtrl as vm'
            });
        // if none of the above states are matched, use this as the fallback
        //$urlRouterProvider.otherwise('/app/segmentos-search');
        $urlRouterProvider.otherwise( function($injector) {
                var $state = $injector.get("$state");
                $state.go("app.segmentos-search");
            });
    });
