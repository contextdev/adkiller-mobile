/**
 * Notification
 * @namespace
 */
(function () {
    'use strict';

    angular
        .module('infoad.auth.services')
        .factory('Notification', Notification);

    Notification.$inject = ['$rootScope', '$cordovaPush', '$cordovaDialogs', '$http', '$state', '$q', 'AuthenticationStorage'
    ];

    function Notification($rootScope, $cordovaPush, $cordovaDialogs, $http, $state, $q, AuthenticationStorage) {

        function registerListener() {
            $rootScope.$on('$cordovaPush:notificationReceived', function (event, notification) {
                if (ionic.Platform.isAndroid()) {
                    handleAndroid(notification);
                }
            });
        }

        function unRegister() {

            var options = {
                "senderID": "713985941525",
                "regID": AuthenticationStorage.get_notification_reg_id(),
                "registration_id": AuthenticationStorage.get_notification_reg_id()
            };
            if (appConfig.push) {
                return $http.put(endpoint_url + '/api-mobile//notification/unregister/',
                    {registration_id: AuthenticationStorage.get_notification_reg_id()
                        , device_id: uuid
                    }).
                    then(function (data){
                        console.log("Success unregistered in Infoad")
                    }, function(error) {
                        console.log("Fail unregistered in Infoad")
                    });
            } else {
                return $q.reject("No Push Implemented");
            }
        }
        // Register
        function register() {
            $http.defaults.headers.common.Authorization = 'Token ' + AuthenticationStorage.get_token();
            registerListener();

            var config = null;

            if (ionic.Platform.isAndroid()) {
                config = {
                    "senderID": "713985941525"
                };
            }
            else if (ionic.Platform.isIOS()) {
                config = {
                    "badge": "true",
                    "sound": "true",
                    "alert": "true"
                }
            }
            if(appConfig.push) {
                $cordovaPush.register(config).then(function (result) {
                    if (ionic.Platform.isIOS()) {
                        var regId = result;
                    }
                }, function (err) {
                    console.log("Register error " + err)
                });
            }
        }

        function handleAndroid(notification) {
            if (notification.event == "registered") {
                if (AuthenticationStorage.get_notification_reg_id() != notification.regid ) {
                    AuthenticationStorage.set_notification_reg_id(notification.regid);
                    $http.post(endpoint_url + '/api-mobile/notification/register/',
                        {registration_id: notification.regid, device_id: uuid }).
                        then(function (data){
                            console.log("Success registerd in infoad");
                        }, function(error) {
                            console.log("Fail registered in infoad");
                        });
                }
            }
            else if (notification.event == "message") {
                $cordovaDialogs.alert(notification.message, "Notificación");
            }
            else if (notification.event == "error"){
                $cordovaDialogs.alert(notification.message, "Push notification error event");
            }

            else {
                console.log( "Push notification handler - Unprocessed Event");
            }
        }

        // Android Notification Received Handler
        var Notification = {
            register: register,
            unRegister: unRegister,
            registerListener: registerListener
        }

        return Notification;

    }
})();