/**
 * AuthenticationStorage
 * @namespace infoad.authe.services
 */
(function () {
    'use strict';

    angular
        .module('infoad.auth.services')
        .factory('AuthenticationStorage', AuthenticationStorage);

    AuthenticationStorage.$inject = ['$localstorage'];

    /**
     * @namespace AuthenticationStorage
     * @returns {Factory}
     */
    function AuthenticationStorage($localstorage) {
        var Authentication = {
            is_authenticated: isAuthenticated,
            get_token: getToken,
            set_token: setToken,
            clean_data: cleanData,
            set_notification_reg_id: setNotificationRegId,
            get_notification_reg_id: getNotificationRegId,
            is_notification_registered: isNotificationRegistered
        };

        var auth = {};

        init();
        function init(){
            auth = {
                currentUserToken: $localstorage.get('currentUserToken')
            };
        }

        function cleanData(){
            auth = {};
            $localstorage.set('currentUserToken', 'undefined');

        }

        return Authentication;

        /**
         * Return boolean to state authenticated
         * @returns {boolean}
         */
        function isAuthenticated(){
           if(auth['currentUserToken'] != 'undefined' &&
               typeof (auth['currentUserToken']) != 'undefined' &&
               typeof (auth['currentUserToken']) != undefined &&
               auth['currentUserToken']){
                return true;
            }
            return false;
        }

        /**
         * Return token
         * @returns {*}
         */
        function getToken(){
            return auth['currentUserToken'];
        }

        /**
         * Set token
         * @param token
         */
        function setToken(token){
            auth['currentUserToken'] = token;
            $localstorage.set('currentUserToken', token);
        }


        /**
         * Set register notification id
         * @param id
         */
        function setNotificationRegId(id){
            auth['NotificationRegId'] = id;
            $localstorage.set('NotificationRegId', id);
        }

        /**
         * get register notification id
         * @returns {*}
         */
        function getNotificationRegId(){
            return auth['NotificationRegId'];
        }

        /**
         *
         * @returns {boolean}
         */
        function isNotificationRegistered(){
            if(auth['NotificationRegId'] != 'undefined' &&
               typeof (auth['NotificationRegId']) != 'undefined' &&
               typeof (auth['NotificationRegId']) != undefined &&
               auth['NotificationRegId']){
                return true;
            }
            return false;
        }

    }

})();