/**
 * Authentication
 * @namespace infoad.auth.services
 */
(function () {
    'use strict';

    angular
        .module('infoad.auth.services')
        .factory('Authentication', Authentication);

    Authentication.$inject = ['$http'];

    /**
     * @namespace User
     * @returns {Factory}
     */
    function Authentication($http) {
        var User = {
            login: login
        };

        return User;

        function login(name, pw){
            return $http.post(endpoint_url + '/api-mobile/api-token-auth/', {username: name, password: pw });
        }
    }

})();