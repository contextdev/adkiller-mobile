/**
 * LoginCtrl
 * @namespace
 */
(function () {
    'use strict';

    angular
        .module('infoad.auth.controllers')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$ionicPopup', '$state','Authentication', 'AuthenticationStorage', '$ionicLoading', '$http', 'Notification'];

    /**
     * @namespace LoginCtrl
     */
    function LoginCtrl($ionicPopup, $state, Authentication, AuthenticationStorage, $ionicLoading, $http, Notification) {
        var vm = this;
        vm.data = {
            'username': '',
            'password': ''
        };

        vm.login = function() {

            if(vm.data.username != '' && vm.data.password != ''){
                $ionicLoading.show({
                    template: '<ion-spinner icon="spiral"></ion-spinner>'
                });
                Authentication.login(vm.data.username, vm.data.password).success(function(data) {
                    AuthenticationStorage.set_token(data.token);
                    $ionicLoading.hide();
                    $http.defaults.headers.common.Authorization = 'Token ' + data.token;
                    Notification.register();
                    console.log("REGISTER NOTIFICATION");
                    $state.go('app.segmentos-search');

                }).error(function(data) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Error!',
                        template: 'Por favor, comprueba los datos de autenticacion.'
                    });
                });
            }else{
                $ionicPopup.alert({
                    title: 'Error!',
                    template: 'Todos los datos son requeridos.'
                });
            }

        };
    }
})();