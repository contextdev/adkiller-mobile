(function () {
    'use strict';

    angular
        .module('infoad.auth', [
            'infoad.auth.services',
            'infoad.auth.controllers'
        ]);
    angular
        .module('infoad.auth.services', []);

    angular
        .module('infoad.auth.controllers', []);
})();