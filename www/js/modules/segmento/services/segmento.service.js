/**
 * Segmento
 * @namespace adkiller.segmento.services
 */
(function () {
    'use strict';

    angular
        .module('infoad.segmento.services')
        .factory('Segmento', Segmento);

    Segmento.$inject = ['$http'];

    /**
     * @namespace Segmento
     * @returns {Factory}
     */
    function Segmento($http, $q) {

        var Segmento = {
            list: list,
            search: search,
            get: detail
        };

        function list(){
            return $http.get(endpoint_url + '/api-mobile/segmentos/');
        }

        function detail(id){
            return $http.get(endpoint_url + '/api-mobile/segmentos/' + id + "/");
        }

        function search(q){
            return $http.get(endpoint_url + '/api-mobile/segmentos/?search=' + q);
        }

        return Segmento;
    }
})()
