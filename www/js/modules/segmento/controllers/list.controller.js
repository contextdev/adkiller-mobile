/**
 * SegmentosListCtrl
 * @namespace
 */
(function () {
    'use strict';

    angular
        .module('infoad.segmento.controllers')
        .controller('SegmentosListCtrl', SegmentosListCtrl);

    SegmentosListCtrl.$inject = ['Segmento', '$ionicLoading'];

    /**
     * @namespace SegmentosListCtrl
     */
    function SegmentosListCtrl(Segmento, $ionicLoading){
        var vm = this;

        vm.endpoint_url = endpoint_url;
        vm.segmentos = [];

        activate();

        function activate(){
            Segmento.list().then(SegmentosListSuccess, SegmentosListError);

            function SegmentosListSuccess(data){
                vm.segmentos = data.data.results;
            }


            function SegmentosListError(data){
                console.log("ERROR")
            }
        }



    }
})();