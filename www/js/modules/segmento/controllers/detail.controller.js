/**
 * AdDetailCtrl
 * @namespace
 */
(function () {
    'use strict';

    angular
        .module('infoad.segmento.controllers')
        .controller('SegmentDetailCtrl', SegmentDetailCtrl);

    SegmentDetailCtrl.$inject = ['Segmento', '$state', '$sce'];

    /**
     * @namespace SegmentDetailCtrl
     */
    function SegmentDetailCtrl(Segmento, $state, $sce) {
        var vm = this;

        vm.endpoint_url = endpoint_url;
        vm.segmento = {};

        activate();

        function activate(){

            // TODO: Cambio de query para contemplar el caso de uso, cuando un detalle sea publico
            Segmento.get($state.params.id).then(segmentoDetailSuccess, segmentoDetailError);

            function segmentoDetailSuccess(data){
                vm.segmento = data.data;
                var created = new Date(vm.segmento.created_at);
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date();
                var days_since = Math.round(Math.abs(created.getTime() - firstDate.getTime())/(oneDay));
                vm.segmento.days_since_created = days_since;
            }

            function segmentoDetailError(data){
                console.log("ERROR : DETALLE");
            }
        }

        vm.get_url = function() {
            return $sce.trustAsResourceUrl(vm.endpoint_url + '/media_map/player/' + vm.segmento.id + '/');
        };

        vm.share = function(){
            var options = {
                message: 'Mira el clip "' + vm.segmento.titulo + '"', // not supported on some apps (Facebook, Instagram)
                subject: 'Mira el clip ' + vm.segmento.titulo, // fi. for email
                files: [vm.segmento.main_thumb],
                url: endpoint_url + vm.segmento.share_url,
                chooserTitle: vm.segmento.titulo // Android only, you can override the default share sheet title
            };

            var onSuccess = function(result) {
                console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
                console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
            };

            var onError = function(msg) {
                console.log("Sharing failed with message: " + msg);
            };

            window.plugins.socialsharing.shareWithOptions(options);
            //window.plugins.socialsharing.share(null, null, 'http://www.compraloahi.com.ar/static/image/compraloahi-logo.png');
        }
    }
})();