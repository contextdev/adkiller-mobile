/**
 * SegmentosSearchListCtrl
 * @namespace
 */
(function () {
    'use strict';

    angular
        .module('infoad.segmento.controllers')
        .controller('SegmentosSearchListCtrl', SegmentosSearchListCtrl);

    SegmentosSearchListCtrl.$inject = ['Segmento', '$rootScope', '$state', '$scope', '$ionicPopover'];

    /**
     * @namespace SegmentosSearchListCtrl
     */
    function SegmentosSearchListCtrl(Segmento, $rootScope, $state, $scope, $ionicPopover){
        var vm = this;

        vm.endpoint_url = endpoint_url;
        vm.segmentos = [];

        vm.q = '';
        activate();

        $rootScope.$on('change.textSearch', function (event, text) {
            if(text != vm.q){
                vm.q = text;
                search();
                if($state.current.name != 'app.segmentos-search') {
                    $state.go('app.segmentos-search');
                }
            }
        });

        function search(){
            Segmento.search(vm.q).then(searchSegmentSuccess, searchSegmentError);

            function searchSegmentSuccess(data){
                vm.segmentos = data.data.results;
            }

            function searchSegmentError(data){
                console.log("ERROR EN LA BUSQUEDA");
            }
        }
        function activate(){
            Segmento.list().then(SegmentosListSuccess, SegmentosListError);

            function SegmentosListSuccess(data){
                vm.segmentos = data.data.results;
            }


            function SegmentosListError(data){
                console.log("ERROR")
            }
        }

        vm.format_time = function(seconds){
            var hours = parseInt(seconds/3600);
            var min = parseInt((seconds % 3600)/60);
            var seg = parseInt((seconds % 3600) % 60);
            var result = '';
            if(hours < 10){
                result += '0' + String(hours);
            }else{
                result += String(hours);
            }
            result += ':';
            if(min < 10){
                result += '0' + String(min);
            }else{
                result += String(min);
            }

            result += ':';
            if(seg < 10){
                result += '0' + String(seg);
            }else{
                result += String(seg);
            }
            return  result;
        };


        vm.go_detail = function(){
            var id = angular.copy(vm.item_open_menu.id);
            vm.closePopover();
            $state.go('app.segmento-detail',{'id': id});
        };


        vm.share = function(item){

            if(item){
                var options = {
                    message: 'Mira el clip "' + angular.copy(item.titulo) + '"',
                    subject: 'Mira el clip ' + angular.copy(item.titulo),
                    files: [angular.copy(item.main_thumb)],
                    url: endpoint_url + angular.copy(item.share_url),
                    chooserTitle: angular.copy(item.titulo)
                };
            }else{
                var options = {
                    message: 'Mira el clip "' + angular.copy(vm.item_open_menu.titulo) + '"',
                    subject: 'Mira el clip ' + angular.copy(vm.item_open_menu.titulo),
                    files: [angular.copy(vm.item_open_menu.main_thumb)],
                    url: endpoint_url + angular.copy(vm.item_open_menu.share_url),
                    chooserTitle: angular.copy(vm.item_open_menu.titulo)
                };
                vm.closePopover();
            }

            window.plugins.socialsharing.shareWithOptions(options);

        };


        vm.format_date = function(date){
            var created = new Date(date);
                var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
                var firstDate = new Date();
                var days_since = Math.round(Math.abs(created.getTime() - firstDate.getTime())/(oneDay));
                return days_since;
        }

        /** START SCRIPT POPOVER **/
// TODO: CREATE DIRECTIVE WITH THIS SCRIPT
        var template = '<ion-popover-view><ion-header-bar> <h1 class="title">My Popover Title</h1> </ion-header-bar> <ion-content> Hello! </ion-content></ion-popover-view>';

        vm.popover = $ionicPopover.fromTemplate(template, {
            scope: $scope
        });

// .fromTemplateUrl() method
        $ionicPopover.fromTemplateUrl('my-popover.html', {
            scope: $scope
        }).then(function(popover) {
            $scope.popover = popover;
        });

        vm.item_open_menu = null;
        vm.openPopover = function($event, item) {
            vm.item_open_menu = angular.copy(item);
            $scope.popover.show($event);
        };
        vm.closePopover = function() {
            vm.item_open_menu = null;
            $scope.popover.hide();
        };

        /** END SCRIPT POPOVER */

    }
})();