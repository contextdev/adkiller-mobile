(function () {
    'use strict';

    angular
        .module('infoad.segmento', [
            'infoad.segmento.controllers',
            'infoad.segmento.services'
        ]);

    angular
        .module('infoad.segmento.controllers', []);

    angular
        .module('infoad.segmento.services', []);
})();