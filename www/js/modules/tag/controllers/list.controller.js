/**
 * TagListCtrl
 * @namespace
 */
(function () {
    'use strict';

    angular
        .module('infoad.tag.controllers')
        .controller('TagListCtrl', TagListCtrl);

    TagListCtrl.$inject = ['Tag', '$ionicLoading'];

    /**
     * @namespace TagListCtrl
     */
    function TagListCtrl(Tag, $ionicLoading){
        var vm = this;

        vm.endpoint_url = endpoint_url;
        vm.tags = [];

        activate();

        function activate(){
            Tag.list().then(TagListSuccess, TagListError);

            function TagListSuccess(data){
                vm.tags = data.data;
                console.log(vm.tags)
            }

            function TagListError(data){
                console.log("ERROR")
            }
        }

    }
})();