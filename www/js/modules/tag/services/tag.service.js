/**
 * Tag
 * @namespace adkiller.tag.services
 */
(function () {
    'use strict';

    angular
        .module('infoad.tag.services')
        .factory('Tag', Tag);

    Tag.$inject = ['$http'];

    /**
     * @namespace Tag
     * @returns {Factory}
     */
    function Tag($http, $q) {

        var Tag = {
            list: list
        };

        function list(){
            console.log(endpoint_url + '/api-mobile/tags/');
            var data = $http.get(endpoint_url + '/api-mobile/tags/');
            console.log(data);
            return data;
        }

        return Tag;
    }
})()
