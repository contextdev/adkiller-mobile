(function () {
    'use strict';

    angular
        .module('infoad.tag', [
            'infoad.tag.controllers',
            'infoad.tag.services'
        ]);

    angular
        .module('infoad.tag.controllers', []);

    angular
        .module('infoad.tag.services', []);
})();