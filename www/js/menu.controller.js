/**
 * MenuController
 * @namespace
 */
(function () {
    'use strict';

    angular
        .module('infoad.menu.controllers', [])
        .controller('MenuController', MenuController);

    MenuController.$inject = ['$rootScope', 'AuthenticationStorage', '$window', '$ionicFilterBar', 'Notification'];

    /**
     * @namespace MenuController
     */
    function MenuController($rootScope, AuthenticationStorage, $window, $ionicFilterBar, Notification) {
        var vm = this;
        vm.filterBarInstance = {};

        vm.showFilterBar = function () {
            vm.filterBarInstance = $ionicFilterBar.show(
                {
                    update: updateTextSearch,
                    delay: 1200,
                    //cancelOnStateChange: false
                }
            );
        };

        vm.search_text = '';

        function updateTextSearch(result, search_text){
            if(vm.search_text != search_text && search_text != undefined){
                vm.search_text = angular.copy(search_text);

                $rootScope.$broadcast('change.textSearch', search_text);
            }
        }

        vm.logout = function() {
            Notification.unRegister();
            AuthenticationStorage.clean_data();
            $window.location.reload(true);
        };

    }
})();